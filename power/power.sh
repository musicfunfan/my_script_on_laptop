#!/bin/bash
  
declare -a setttings=("poweroff" "reboot" "logout" "exit_menu")

path=$(printf  '%s\n' "${setttings[@]}"  |dmenu -i -l 4 -p 'type: ' )

echo "$path"

if [ "$path" == "poweroff" ]
then
   poweroff
fi 

if [ "$path" == "reboot" ]
then
    reboot
fi

if [ "$path" == "logout" ]
then 
    i3-msg exit
fi

if [ "$path" == "exit_menu" ]
then
    exit 1
fi

if [ -z "$path" ]
then 
    
    notify-send "power" "nothing selected" 
    exit 1
fi
