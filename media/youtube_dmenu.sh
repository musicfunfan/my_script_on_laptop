#!/bin/bash

#This is a script that lets you navigate from dmenu and listen or watch
#videos from mpv and yt-dlp and dmenu

#This is a method to save the basic locations in the memory
initialized ()
{
    echo "initialization started"
    locationScript="/home/musicfan/.config/scripts"
    locationMpv=$(whereis mpv | awk '{print $2}')
    modes=(
            "choose from history"
            "something else"
            "change media playing quality"
            "change media playing quality of the playlist"
            "play all in order"
            "play all in random order"
            "Create Custom Playlist"
            "Load old play list"
)
    
    choiceVideo=$(printf "%s\n" "${modes[@]}" | dmenu -i -l 30: -p 'YouTube')
    mode=$(echo "$choiceVideo" | awk '{print $1}')
    echo "This is the choiceVideo $choiceVideo"
    echo "This is the mode $mode"
    export TIMERUN=$(( -1 ))
    echo "This is the TIMERUN $TIMERUN"
    if [ "$mode" != "saved" ] && [ "$choiceVideo" != "something else"  ] && [ "$mode" != "play" ] && [ "$mode" != "play all in random order" ] && [ "$mode" != "something" ] && [ "$choiceVideo" != "change media playing quality" ] && [ "$choiceVideo" != "change media playing quality of the playlist" ] && [ "$choiceVideo" != "Create Custom Playlist" ] && [ "$choiceVideo" != "choose from history" ] && [ "$choiceVideo" != "Load old play list" ] 
    then
        yad --text-info --text="You can choiceVideo for somthing using somthing else 
        option \n or you can select a option from the histroy (if you a histroy) \n try that"
        exit 1
    fi
    echo "initialization done"
}

#This is a method to load all the links in file
loadAllTheLinks ()
{
    echo "this is in development"
    playList=$(cat  $locationScript/historyUrls.txt | cut -d ' ' -f2-)
    echo "$playList" >> "$locationScript"/playList.txt
    echo "This is the playlist $playList"
}

#This is a function to get the links form YouTube
searchYt ()
{
    echo "Start searching"
    
    if [ "$1" != more ]
    then
        searchActual=$(rofi -dmenu -i -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -p 'search on YouTube : ')
    fi
    
    if [ -z "$searchActual"  ]
    then
        exit 1 
    fi
    
    echo "this is the searchActual $searchActual "
    searchResults=$(python3 "$locationScript"/searchYt.py "$searchActual")
    echo "this is the searchResults $searchResults"
    export TIMERUN=$(( TIMERUN + 10 ))
    echo "This is the TIMERUN $TIMERUN"
    echo "done searching"
}

#This is the method to load the data from the history files
loadFiles ()
{
    #Load the files
    historyTitleList=$(less "$locationScript/historyTitle.txt")
    historyUrlsList=$(less "$locationScript/historyUrls.txt")
    echo "This is the historyTitleList $historyTitleList"
    echo "This is the historyUrlsList $historyUrlsList"
}

#Find all the links from the history
findUrlsFromHistory ()
{
    #Count the rows of the historyTitle file
    countRowsTitleFile=$(less "$locationScript"/historyTitle.txt | wc -l) 
    echo "This is the countRowsTitleFile $countRowsTitleFile"

    #Count the rows of the historyUrls file
    countRowsUrlsFile=$(less "$locationScript"/historyUrls.txt | wc -l)
    echo "This is the countRowsUrlsFile $countRowsUrlsFile " 

    #Fint all the Titles from history
    for ((t=0; t<="$countRowsTitleFile"; t++))
    do 
        listToArray=$(echo "$historyTitleList" | awk "FNR == $t")
       resultTitlesArray+=("$listToArray")
    done    
    
    echo "This is the aray Title from history" "${resultTitlesArray[@]}"

    #Fint all the Urls from history
    for ((u=0; u<="$countRowsUrlsFile"; u++))
    do 
        listToArray=$(echo "$historyUrlsList" | awk "FNR == $u")
       resultLinksArray+=("$listToArray")
    done
    
    echo "This is the aray url from history" "${resultLinksArray[@]}"  
    num=${#resultLinksArray[@]} 
}

#This function is to find all the urls for the titles
searchForUrlsNet ()
{   
    echo "Search fot urls starting"
    resultLinksArray=()
    resultTitlesArray=() 
    array=()
    
    #Find all titles
    for a in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
    do  
        listToArray=$(echo "$searchResults" | awk "FNR ==$a")
        array+=("$listToArray")
    done

    for a in {0..20..2}
    do 
        resultsTitles=$(echo -e "${array[$a]}")
        resultTitlesArray+=("$resultsTitles")
    done
    resultTitlesArray+=(more)
    echo "this is the resultsTitlesAraay" "${resultTitlesArray[@]}"

    #Find all the urls
    for l in {1..21..2}
    do 
        resultLinks=$(echo -e "${array[$l]}")
        resultLinksArray+=("$resultLinks")
    done
    
    echo "This is the resultsUrlsAray" "${resultLinksArray[@]}"
    num=${#resultTitlesArray[@]}
    echo "this is the number for search web net $"
    echo "Search for urls complited"
}

#This is to print all the availible results to the dmenu to prompt the user
resultDmenu ()
{   
    echo "Result dmenu started"


    if [ "$1" == "historyMode" ]
    then
        choiceVideo=$(less "$locationScript/historyTitle.txt" | rofi -i -dmenu -l 30 -theme "$HOME/.config/rofi/second_theme/dmenu_edited.rasi")
    fi

    
    test2=$(less "$locationScript"/historyTitle.txt )
    createPlaylistOptions=("Playlist Completed" "$test2")
    if [ "$1" == "customPlaylist" ]
    then
        while [ "$choiceVideo" != "Playlist Completed" ]
        do 
            choiceVideo=$( printf "%s\n" "${createPlaylistOptions[@]}"   | dmenu -i -l 25 -p 'YouTube')
            
            if [ "$choiceVideo" != "Playlist Completed" ]
            then

                playlistArray+=("$choiceVideo")
                echo "this is the playlist list"
                printf "%s\n" "${playlistArray[@]}"
            fi
        done
    fi
    
    if [ "$1" == "listResult" ] 
    then
       choiceVideo=$(printf "%s\n" "${resultTitlesArray[@]}" | dmenu -i -l "$num"  -p "Choose a result")
    fi
    
    if [ "$1" == "listQualityVideo" ] 
    then
        num=$(( "$lineCount" + 1 )) 
        videoQuality=$(printf "%s\n" "${arrayQuality[@]}"  | dmenu -i -l "$num" -p "Choose a video quality")
    fi
    
    if [ "$1" == "listQualityAudio" ] 
    then
        num=$(( "$lineCount" + 1 )) 
        audioQuality=$(printf "%s\n" "${arrayQuality[@]}"  | dmenu -i -l "$num" -p "Choose a audio quality")
    fi
    
    echo "results to dmenu completed"
}

#Link all urls to all Titles for the new stuff
linksUrls ()
{    
    echo "This is the choice video inside LinkUrls $choiceVideo"
    echo
    echo "This is the resultsTitlesArray inside linkUrls" "${resultTitlesArray[@]}"
    
    for i in "${!resultTitlesArray[@]}";
    do
        if [[ "${resultTitlesArray[$i]}" = "${choiceVideo}" ]];
        then
            index=$i
            break
        fi
    done 
   
    if [ "$index" -gt -1 ]
    then
        echo "Indexing maching url finded"
        echo "${resultTitlesArray[$index]}"
        echo "This is the links"
        echo "${resultLinksArray[$index]}"
        echo "With the index $index"
        savedToPlayOne=$(echo "${resultLinksArray[$index]}" | cut -d" " -f2)
    else
        echo "$choiceVideo is not in Array."    
    fi
    
    echo "this is the choice video: $choiceVideo"
}

#This is a mehtod to save new searches in the disk
saveNewSearches ()
{
    echo "Saving new searches started"
    
    if [ -n "$choiceVideo" ]
    then
        echo "This is in the saveNewSearches "
        echo "saved $choiceVideo" >> "$locationScript/historyTitle.txt"
        echo "saved ${resultLinksArray[$index]}" >> "$locationScript/historyUrls.txt"
    fi

    #Removing doubles from the history files
    awk '!x[$0]++' $locationScript/historyTitle.txt >> ./historyTitle2.txt
    awk '!x[$0]++' $locationScript/historyUrls.txt >> ./historyUrls2.txt
    mv $locationScript/historyTitle2.txt $locationScript/historyTitle.txt
    mv $locationScript/historyUrls2.txt $locationScript/historyUrls.txt 

    echo "Saving new searces completed"
}   

#This method finds all the quality options for the curent link
findQualityOptions ()
{
    if [ "$choiceVideo" == "change media playing quality" ] 
    then
        echo "this is the media to change" "$mediaToChangeQuality"
        mediaToChangeQuality=$savedToPlay
    fi

    if [ "$mode" == "choose" ] || [ "$mode" == "something" ]
    then
        mediaToChangeQuality=$(echo "${resultLinksArray[$index]}" | cut -d" " -f2)
    fi
   
    echo "This is the the media to change quality $mediaToChangeQuality"
    qualityList=$(yt-dlp "$mediaToChangeQuality" -Fq)
    echo "${resultLinksArray[$index]}"
    lineCount=$(echo "$qualityList" | wc -l)
    echo "This is the quality option: $qualityList"
    echo "This is the line count $lineCount"
    
    for ((c=0; c<="$lineCount"; c++))
    do 
        listToArray=$(echo "$qualityList" | awk "FNR == $c")
        arrayQuality+=("$listToArray")
    done

echo "this is $savedToPlay"
}

#Get the codes for the quality choice 
getTheQualityCodes ()
{
    choiceVideoQuality=$(echo "$videoQuality" | awk '{printf $1}')
    choiceAudioQuality=$(echo "$audioQuality" | awk '{printf $1}')
    
    if [ -z "$choiceAudioQuality"  ] || [ -z "$choiceVideoQuality" ]
    then 
        choiceAudioQuality="best"
        choiceVideoQuality="best"
    fi
    
    echo "$choiceVideoQuality this is video"
    echo "$choiceAudioQuality this is audio"
}

#play the video that loaded with mpv media player via mpv
playVideo ()
{
    if [ "$1" == "oldPlaylist" ]
    then
        "$locationMpv" --playlist="$oldPlaylistChoice" --playlist-start="$2"
    fi
    
    if [ "$1" == "customPlaylist" ]
    then
        echo "This is playling new playlist"
        "$locationMpv" --ytdl-format="$choiceVideoQuality"+"$choiceAudioQuality" --playlist="$locationScript/playList.txt" --save-position-on-quit --no-resume-playback 
    fi
    
    if [ "$1" == "resume" ]
    then 
        echo "This is the resume"
        "$locationMpv" --ytdl-format="$choiceVideoQuality"+"$choiceAudioQuality" "$savedToPlay" --save-position-on-quit
    fi

    if [ "$1" == "resumePlayList" ]
    then 
        echo "This is the resume playlist"
        "$locationMpv" --ytdl-format="$choiceVideoQuality"+"$choiceAudioQuality" --playlist="$locationScript/playList.txt" --save-position-on-quit
    fi
    #
    if [ "$1" != "playLIstInOrder" ] && [ "$1" != "playLIstInRundomOrder" ] && [ "$1" != "resume" ] && [ "$1" != "customPlaylist" ] && [ "$1" != "oldPlaylist" ] 
        then
        echo "Start Playing"
        "$locationMpv" --ytdl-format="$choiceVideoQuality"+"$choiceAudioQuality" "$savedToPlayOne" --save-position-on-quit --no-resume-playback 
        echo "Stop Playing"
    fi
    
    if [ "$1" == "playLIstInOrder" ]
    then
        echo "Start Playing"
        "$locationMpv" --playlist=$locationScript/playList.txt --save-position-on-quit --no-resume-playback 
        echo "Stop Playing"
    fi

    if [ "$1" == "playLIstInRundomOrder" ]
    then
        echo "Start Playing"
        "$locationMpv" --playlist=$locationScript/playList.txt --shuffle --save-position-on-quit --no-resume-playback 
        echo "Stop Playing"
    fi  
}

#deleting old files
deleteOldFiles ()
{
    echo "Deleting the content of the playList.txt file"
    truncate -s 0 $locationScript/playList.txt
    truncate -s 0 $locationScript/playList.txt
    echo "Done deleting the content of the .nowPlaying file"
}

#Saves the file the media to resume playback
saveNowPlay ()
{
    if [ "$1" != "playlistMode" ]
    then
        echo "Normal mode"
        savedToPlay=$(playerctl metadata | grep url | awk '{printf $3}')
        echo "This is the normal mode link $savedToPlay"
    fi

    if [ "$1" == "playlistMode" ]
    then
        echo "Playlist mode"
        savedToPlay=$(cat $locationScript/playList.txt)
        mediaToChangeQuality=$(playerctl metadata | grep url | awk '{printf $3}')
        echo "This is the media to change quality $mediaToChangeQuality"
    fi
}

#delete the old watch_later from the mpv
deletePreviuseSavedMpvState ()
{
    rm -r "$HOME"/.config/mpv/watch_later
}

#Create the new custom playlist
createNewCustomPlaylist ()
{
    echo "This is in new custom playlist function"
    echo "creating"
    echo "${resultLinksArray[$index]}" | cut -d" " -f2 >> "$locationScript"/playList.txt
    echo "This is the the link to save"
    echo "done creating"
}

#Save the custom playlist
saveCustomPlaylist ()
{
    yesNo=("Yes" "No")
    savePlaylist=$(printf "%s\n" "${yesNo[@]}" | rofi -dmenu -l 2 -theme "$HOME/.config/rofi/second_theme/dmenu_edited.rasi" -p "Do you want to save the playlist :")
    
    if [ "$savePlaylist" == "Yes" ]
    then
        mkdir "$locationScript/customPlaylists"
        nameOfPlaylist=$(echo "exit_menu" | rofi -dmenu -i -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -p "Give a name for the playlist :")
        if [ "$nameOfPlaylist" == exit_menu ]
        then
            exit 1
        else
            cp "$locationScript"/playList.txt "$locationScript/customPlaylists/$nameOfPlaylist.txt"
        fi
    else
        dunstify -a "Media" "YouTube script" "Playlist will not be saved"
    fi
}


loadOldPlaylist ()
{
    oldPlaylistChoice=$(dmenufm -t $HOME/.config/scripts/customPlaylists)
}    


saveToGenerelaPlaylist ()
{
    echo "This is in development"
    echo "This is is the save to general playlist"
    bash "$locationScript/saveToPlaylist.sh" "$1"
}


readOldPlaylist ()
{
    mapfile LinksAray < "$oldPlaylistChoice"
    echo "This is the $oldPlaylistChoice"
    titlesArray=()
    notify-send "YouTube Script" "Loading Titles"
    playListName=$(echo "$oldPlaylistChoice" | sed 's:.*/::')
    echo "This is the playListName $playListName"
    mkdir $HOME/.config/scripts/customPlaylist/".$playListName"
    linesAlreadyWithTitles=$(cat "$oldPlaylistChoice"cashe | wc -l)

    if [  "$linesAlreadyWithTitles" != 0  ]
    then
       for links in ${LinksAray[@]}
       do
           mapfile allLinks < "$oldPlaylistChoice"
           echo "This is the oldPlaylistChoice $oldPlaylistChoice"
       done


       preCashedTitles=$(cat "$oldPlaylistChoice"cashe | wc -l)
       echo "This is the preCashedTitles $preCashedTitles"
       cat "$oldPlaylistChoice"
       allLinksNumber=$(cat "$oldPlaylistChoice" | wc -l)
       allLinks=$(cat "$oldPlaylistChoice")
       echo "This is the preCashedLinks number $allLinksNumber"
       cat "$oldPlaylistChoice"cashe
       diff=$(( "$allLinksNumber" - "$preCashedTitles" ))
       echo "This is the diff $diff"
       steps=$(( "$diff" -1 ))
       newIndex=0
       index=$(( "$preCashedTitles" ))
       z=0
       newTitles=()

       if [ "$diff" != 0 ]
       then
           noCashed=()
           mapfile titlesArray < "$oldPlaylistChoice"cashe
           for link in $(seq 0 $steps)
           do
               lineToAdd=$(( "$allLinksNumber" - "$link" ))
               echo "This is the lineToAdd $lineToAdd"
               echo "This is the for loop for to add new stuff to the allLinksNumber"
               echo "This is the link $link"
               noCashed[$z]+=$(printf "%s\n" "${allLinks[@]}" | awk -v lineToAdd=$lineToAdd 'NR==lineToAdd')
               z=$(( z=$z+1 ))
            done

           echo "This is the noCashed ${noCashed[@]}"
           for link in ${noCashed[@]}
           do
               echo "Now working the for loop inside the cashe for"
               titles=$(yt-dlp --get-title "$link")
               yt-dlp --skip-download --write-thumbnail $link -o $HOME/.config/scripts/customPlaylists/".$playListName"/"$index $titles"
               newTitles[$newIndex]+="$index $titles"
               titlesArray[$index]+="$index $titles"
               index=$(( $index + 1 ))
               newIndex=$(( $newIndex + 1 ))
               echo "This is the newTitles ${newTitles[@]}"
           done
           printf "%s\n" "${newTitles[@]}" >> "$oldPlaylistChoice"cashe
       else
           mapfile titlesArray < "$oldPlaylistChoice"cashe
       fi
    else
        i=0
        for link in ${LinksAray[@]}
        do
            echo "This is for not titles saved"
            titles=$(yt-dlp --get-title "$link")
            yt-dlp --skip-download --write-thumbnail $link -o $HOME/.config/scripts/customPlaylists/".$playListName"/"$i $titles"
            titlesArray["$i"]+="$i $titles"
            i=$(( $i+1 ))
        done
        printf "%s\n" "${titlesArray[@]}" >> "$oldPlaylistChoice"cashe
    fi
}

playlistMode()
{
    choiceYesNo=("yes" "no")
    playAll=$(printf "%s\n" "${choiceYesNo[@]}" | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi  -l 2 -p "Play all ?")
    echo "This is the plauAll $playAll"
}

selectMediaFromPlaylist ()
{
        notify-send "YouTube script" "Done"
        echo "${titlesArray[@]}"
        whatToPlayFromPlaylist=$(nsxiv -to $HOME/.config/scripts/customPlaylists/".$playListName" | sed 's:.*/::')
        playlistPos=$(echo $whatToPlayFromPlaylist | awk '{printf $1}')
        echo " this is the playlistPos $playlistPos"
}


#Calling functions from above
initialized
if [ "$mode" != "saved" ] && [ "$choiceVideo" == "something else" ]
then
    searchYt
    searchForUrlsNet
    resultDmenu listResult
    
    while [ "$choiceVideo" == "more" ]
    do
        searchYt more
        searchForUrlsNet
        resultDmenu listResult
    done    
    saveToGenerelaPlaylist "${resultLinksArray[$index]}"
    linksUrls net
    saveNewSearches
    findQualityOptions 
    resultDmenu listQualityVideo
    resultDmenu listQualityAudio
    getTheQualityCodes
    playVideo net
fi

#What to do when user loads links form the history
if [ "$choiceVideo" == "choose from history" ]
then
    resultDmenu historyMode
    deleteOldFiles
    loadFiles
    findUrlsFromHistory
    linksUrls 
    findQualityOptions 
    resultDmenu listQualityVideo
    resultDmenu listQualityAudio
    getTheQualityCodes
    playVideo 
fi

#What to do if user selects play all in order 
if [ "$mode" != "saved" ] && [ "$choiceVideo" == "play all in order" ]
then
    deleteOldFiles
    loadAllTheLinks
    getTheQualityCodes
    playVideo playLIstInOrder 
fi

#What to do if user selects play all in order 
if [ "$mode" != "saved" ] && [ "$choiceVideo" == "play all in random order" ]
then
    deleteOldFiles
    loadAllTheLinks
    getTheQualityCodes
    playVideo playLIstInRundomOrder 
fi

#This what to do when user wants to change the quality on the fly
if [ "$choiceVideo" == "change media playing quality of the playlist" ]
then
    deletePreviuseSavedMpvState
    saveNowPlay playlistMode
    sleep 4
    killall mpv
    findQualityOptions
    resultDmenu listQualityVideo
    resultDmenu listQualityAudio
    getTheQualityCodes
    playVideo resumePlayList
    deleteOldFiles
fi

#This what to do when user wants to change the quality on the fly
if [ "$choiceVideo" == "change media playing quality" ]
then
    deletePreviuseSavedMpvState
    saveNowPlay 
    sleep 4
    killall mpv
    findQualityOptions
    resultDmenu listQualityVideo
    resultDmenu listQualityAudio
    getTheQualityCodes
    playVideo resume 
    deleteOldFiles
fi

#This is the what to do when user want to create custom playlist
if [ "$choiceVideo" == "Create Custom Playlist" ]
then
    deleteOldFiles
    loadFiles
    findUrlsFromHistory
    resultDmenu customPlaylist
    echo "This is the array before the loop"
    printf "%s\n" "${playlistArray[@]}"
    
    for items in "${playlistArray[@]}"
    do
        choiceVideo="$items"
        echo "This is what will link to a url $items"
        linksUrls
        createNewCustomPlaylist
    done
    getTheQualityCodes
    saveCustomPlaylist
    playVideo customPlaylist
    
fi

# This is to play old playlist
if [ "$choiceVideo" == "Load old play list" ]
then
    deletePreviuseSavedMpvState
    loadOldPlaylist
    readOldPlaylist
    playlistMode
    while [ "$playAll" == "yes" ]
    do
        selectMediaFromPlaylist
        playVideo oldPlaylist "$playlistPos"
        playlistMode
    done
fi

exit 1
