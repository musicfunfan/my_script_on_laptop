#!/bin/bash

#This is a script to stream my favorites FM station via mpv

#load the radio stations
loadRadio ()
{
    radios=("https://rockfm.gr" "http://85.10.206.222:8012/stream.mp3" "https://www.redfm.gr/player/")
    RockFM="${radios[0]}"
    Red="${radios[2]}"
    Atlantis="${radios[1]}"

}

dmenuResults ()
{
    choice=$(printf "%s\n" "RockFM" "Red" "Atlantis" "Stop radio" | rofi -dmenu -i -l 4 -theme "$HOME/.config/rofi/second_theme/dmenu_edited.rasi" -p "Choose the radio station")
}

#Play the selected station 
playFM ()
{
    if [ "$choice" == "RockFM" ]
    then
        killall mpv
        mpv "$RockFM"
    fi

    if [ "$choice" == "Red" ]
    then
        killall mpv
        mpv "$Red"
    fi

    if [ "$choice" == "Atlantis" ]
    then 
        killall mpv
        mpv "$Atlantis"
    fi
}


#This is to play radiofm
loadRadio
dmenuResults
playFM

if [ "$choice" == "Stop radio" ]
then 
    killall mpv
    exit 1
fi

exit 1
