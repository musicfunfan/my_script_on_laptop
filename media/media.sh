#!/bin/bash

places=("video" "YouTube" "music" "photos" "media_url" "media_urls_list" "save to playlist" "Radio FM" "exit_menu")
options=("play all" "play all shuffle" "exit_menu")
path=$(printf "%s\n" "${places[@]}"|dmenu -i -l 20 -p 'select media: ' )
locationOfScript="$HOME/.config/scripts"

createThumbs ()
{
    echo "This is to create thumbs"
    shopt -s nullglob
    titles=("$locationPath"/*)
    mkdir "$locationPath/.thumps"
    for title in "${titles[@]}"
    do

        nameOfTheFile=$(echo $title | sed 's:.*/::')
        cashe=$(ls "$locationPath/.thumps/$nameOfTheFile" | sed 's:.*/::')
        echo "This is the cashe $cashe"
        file=$(ls "$locationPath/$nameOfTheFile" | sed 's:.*/::')
        echo "This is the file $file"
        if [ "$cashe" != "$file"  ]
        then
            echo "Creating thumpnail...."
            ffmpegthumbnailer -i "$title" -o "$locationPath/.thumps/$nameOfTheFile" -q 10 -s 0
        else
            echo "Thumbnail already exits"
        fi
    done

}



if [ "$path" == "video" ]
then 
    
    locationPath=$(rofi -show file-browser-extended -theme "$HOME"/.config/rofi/decu_edited.rasi -q -file-browser-stdout -file-browser-dir /media/$USER/hdd/video)
    echo "This is the locationPath $locationPath"
    createThumbs
    selectThis=$(nsxiv -to "$locationPath/.thumps/")
    playThis=$(echo $selectThis | sed 's:.*/::' )
    echo "This is the playThis $playThis"

    if [ $playThis -z ]
    then
        exit 1
    fi

    mpv $locationPath/$playThis

fi
    
if [ "$path" == "music" ]
then 
    locationPath=$(rofi -show file-browser-extended -q -theme "$HOME"/.config/rofi/decu_edited.rasi -file-browser-stdout -file-browser-dir /media/$USER/hdd/Μουσική)
    echo "This is the locationPath $locationPath"
    options+=( "$(find "$locationPath")" )

    choice=$(printf "%s\n" "${options[@]}"|dmenu -i -l 20 -p 'select media: ' )
    
    if [ "$choice" == "play all" ]
    then    
        mpv "$locationPath" 
    fi  
    
    if [ "$choice" == "play all shuffle" ]
    then    
        mpv "$locationPath" --shuffle
    fi

    if [ "$choice" == "exit_menu" ]
    then
        exit 1
    fi 
   
    if [ "$choice" != "play all" ] && [ "$choice" != "play all shuffle" ] && [ "$choice" != "exit_menu" ]
    then
        mpv "$locationPath"
    fi

    exit 1
fi

if [ "$path" == "photos" ]
then 

    locationPath=$(rofi -show file-browser-extended -q -theme "$HOME"/.config/rofi/dmenu.rasi -file-browser-stdout -file-browser-dir /media/$USER/hdd/Εικόνες)
    echo "This is the locationPath $locationPath"
    nsxiv -to "$locationPath"
fi


if [ "$choice" == "exit_menu" ]
then
    exit 1

fi

if [ "$path" == "YouTube" ]
then 
    /home/$USER/.config/scripts/youtube_dmenu.sh
    exit 1
fi

if [ "$path" == "media_url" ]
then
    locationPath=$(rofi -dmenu -i -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -p 'paste-here the url:')

    bash "$locationOfScript/saveToPlaylist.sh" "$locationPath"
    
    
    if [ "$locationPath" != "''" ]
    then
        echo "This is the url $locationPath"
        mpv "$locationPath" --save-position-on-quit 
    fi
    
    exit 1
fi

if [ "$path" == 'media_urls_list' ]
then
    locationPath=$(dmenufm -t)
    printf "%s\n" "${options[@]}" > $locationOfScript/bookmarkList.txt
    python3 "$locationOfScript/readLinks.py" >> $locationOfScript/bookmarkList.txt "$locationPath"
    choice=$(cat $locationOfScript/bookmarkList.txt |rofi -dmenu -i -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -l 20 -p "Choose link")
    
    if [ "$choice" == "play all" ]
    then
        mpv --playlist="$locationOfScript/bookmarkList.txt" --no-resume-playback
    fi

    if [ "$choice" == "play all shuffle" ]
    then
        mpv --playlist="$locationOfScript/bookmarkList.txt" --shuffle --save-position-on-quit --no-resume-playback
    fi

    if [ "$choice" == "exit_menu" ]
    then
        rm $locationOfScript/bookmarkList.txt
        exit 1
    fi

    if [ "$choice" != "play all" ] && [ "$choice" != "play all shuffle" ] && [ "$choice" != "exit_menu" ]
    then
        mpv "$choice" --save-position-on-quit --no-resume-playback
    fi
    rm $locationOfScript/bookmarkList.txt
    exit 1
fi

if [ "$path" == "Radio FM" ]
then
    "$locationOfScript/radio_dmenu.sh"
fi

if [ "$path" == 'save to playlist' ]
then 
    locationPath=$(rofi -dmenu -i -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -p 'What you want to save:')
    "$locationOfScript/saveToPlaylist.sh" "$locationPath"
    dunstify -a "Media" "Media script" "Saved ➡ $locationPath"
fi


dunstify -a "Media" "Media script" "stop playing"


exit 1
