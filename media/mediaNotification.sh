#!/bin/bash

# This is a script for the media notification 
while getopts 'pnbste' OPTION; do
  case "$OPTION" in
   
    s)
      Status=$(playerctl status)
      MetaData=$(playerctl metadata | cut -d ":" -f2- | awk 'FNR <= 4' | awk '(NR>1)' | tr -s ' ')
      Icon=$(playerctl metadata | awk 'FNR ==5 {print $3}' | awk '{ print substr( $0,8 ) }')
      dunstify -h string:x-dunst-stack-tag:media -i "$Icon" -a "Media" "$Status" "$MetaData" -t 3000
      ;;
    p)
      playerctl play-pause
      sleep 0.1;
      Status=$(playerctl status)
      MetaData=$(playerctl metadata | cut -d ":" -f2- | awk 'FNR <= 4' | awk '(NR>1)' | tr -s ' ')
      Icon=$(playerctl metadata | awk 'FNR ==5 {print $3}' | awk '{ print substr( $0,8 ) }')
      dunstify -h string:x-dunst-stack-tag:media -i "$Icon" -a "Media" "$Status" "$MetaData" -t 3000
      ;;
    n)
      playerctl next
      sleep 1;
      Status='Now Playing:'
      MetaData=$(playerctl metadata | cut -d ":" -f2- | awk 'FNR <= 4' | awk '(NR>1)' | tr -s ' ')
      Icon=$(playerctl metadata | awk 'FNR ==5 {print $3}' | awk '{ print substr( $0,8 ) }')
      dunstify -h string:x-dunst-stack-tag:media -i "$Icon" -a "Media" "$Status" "$MetaData" -t 3000
      ;;
    b)
      playerctl previous 
      sleep 1;
      Status='Now Playing:'
      MetaData=$(playerctl metadata | cut -d ":" -f2- | awk 'FNR <= 4' | awk '(NR>1)' | tr -s ' ' )
      Icon=$(playerctl metadata | awk 'FNR ==5 {print $3}' | awk '{ print substr( $0,8 ) }')
      dunstify -h string:x-dunst-stack-tag:media -i "$Icon" -a "Media" "$Status" "$MetaData" -t 3000
      ;;
    t)
      playerctl stop
      sleep 1;
      Status='stop'
      MetaData=$(playerctl metadata | cut -d ":" -f2- | awk 'FNR <= 4' | awk '(NR>1)' | tr -s ' ' )
      Icon=$(playerctl metadata | awk 'FNR ==5 {print $3}' | awk '{ print substr( $0,8 ) }')
      dunstify -h string:x-dunst-stack-tag:media -i "$Icon" -a "Media" "$Status" "$MetaData" -t 3000
      ;;
    e)
      exit 1
      ;;
    ?)
      echo "Nothing"
      exit 1
      ;;
  esac
done
