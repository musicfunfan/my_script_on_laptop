# This is the Dependencies my scripts need

* dmenu 
* rofi
* maim
* dunst
* playerctl
* i3wm
* radeontop
* feh 
* mpv-mpris (mpv with the plug in to d-bus support) 
* xdotool
* zenity
* dmenufm
* rofi file-browser-extended
* git
* rsync
* jq
* lsb-release 
