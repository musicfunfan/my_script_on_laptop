#!/usr/bin/bash

#This is a script that can short files with filters (i hope)

declare -a HeightToExtract
declare -a WidthToExtract
declare -a titleToExtract
declare -a sizeFileToExtract

loadDirectory()
{
    path=$(rofi -show file-browser-extended -q -theme "$HOME"/.config/rofi/dmenu.rasi -file-browser-stdout -file-browser-dir "$HOME" -file-browser-only-dirs )
    shopt -s nullglob
    filesInDirectory=("$path"/*)
    workingDirectory=$(dirname "$path")
}

loadFile()
{
    path=$(rofi -show file-browser-extended -q -theme "$HOME"/.config/rofi/dmenu.rasi -file-browser-stdout -file-browser-dir "$HOME")
    filesInDirectory=()
    workingDirectory=$(dirname "$path")
    fileInDirectory=$(echo "$path" | sed 's|.*/||')
    filesInDirectory+=("$fileInDirectory")
}

extractRes()
{
    for files in "${filesInDirectory[@]}"
    do
        WidthToExtract+=$(mediainfo "$workingDirectory/$files" | grep "Width" | awk {'print $3 $4 $5'})
        HeightToExtract+=$(mediainfo "$workingDirectory/$file" | grep "Height" | awk {'print $3 $4 $5'})
    done
}

extractTitles()
{
    p=0
    for paths in "${filesInDirectory[@]}"
    do
        titleToExtract=($paths)
    done
}

extractFileSize()
{
    for file in "${filesInDirectory[@]}"
    do
        sizeFileToExtract+=$(mediainfo "$workingDirectory/$file" | grep "File size" | awk {'print $4 $5'})
    done
}

CreateIds()
{
    echo "This is the index number ${#titleToExtract[*]}"
    i=0
    for titles in "${titleToExtract[@]}"
    do
#        echo "This is the titles without sed $titles"
        titles=$(echo "$file" | sed 's|.*/||')
        mkdir -p "$workingDirectory"/.stats/
        rm "$workingDirectory"/.stats/"Stats.txt"
        touch "$workingDirectory"/.stats/"$titles"Stats.txt
        echo ${titleToExtract[$i]} >> "$workingDirectory"/.stats/"$titles"Stats.txt
        echo ${sizeFileToExtract[$i]} >> "$workingDirectory"/.stats/"$titles"Stats.txt
        echo ${HeightToExtract[$i]} >> "$workingDirectory"/.stats/"$titles"Stats.txt
        echo ${WidthToExtract[$i]} >> "$workingDirectory"/.stats/"$titles"Stats.txt
        i=$(( $i + 1 ))
    done
}

ShortStats()
{
    echo "This is the ShortStash method not working for now."
}

selectMode()
{
    options=("Load File" "Load Directory")
    mode=$(printf "%s\n" "${options[@]}" | rofi -dmenu -l 2 -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -p "Select Mode" )
}

selectMode

if [ "$mode" == "Load File" ]
then
    loadFile
elif [ "$mode" == "Load Directory" ]
then
    loadDirectory
fi

extractTitles
extractFileSize
extractRes
CreateIds
ShortStats
