#!/bin/bash

#This is a sctipt to show the volume value in a notification when this change by the user.
#debendencies: pamixer,bash,mediaNotification.sh 


#Set new voolume value taking into account the curent volume
playerctlStatus=$(playerctl status)
muteValue=$(pamixer --get-mute)
newValue=$(pamixer --get-volume)
while getopts 'udm' OPTION; do
    case "$OPTION" in

        u)
            pamixer -i 2
            if (( "$newValue" > 65 ))
            then
                dunstify -h string:x-dunst-stack-tag:volume "$newValue % 🔊 " -a "Volume" -h int:value:"$newValue" -t 2000 -u critical
            else
                dunstify -h string:x-dunst-stack-tag:volume "$newValue % 🔉 " -a "Volume" -h int:value:"$newValue" -t 2000
            fi

            if [ -z "$playerctlStatus" ]
            then
               ~/.config/scripts/mediaNotification.sh -e
            else
                ~/.config/scripts/mediaNotification.sh -s
            fi
            ;;

        d)
            #Volume down by 2 db 
            pamixer -d 2
            if (( "$newValue" > 65 ))
            then
                dunstify -h string:x-dunst-stack-tag:volume "$newValue % 🔊 " -a "Volume" -h int:value:"$newValue" -t 2000 -u critical
            else
                dunstify -h string:x-dunst-stack-tag:volume "$newValue % 🔉 " -a "Volume" -h int:value:"$newValue" -t 2000
            fi

            if [ -z "$playerctlStatus" ]
            then
               ~/.config/scripts/mediaNotification.sh -e
            else
                ~/.config/scripts/mediaNotification.sh -s
            fi
            ;;

        m)
            
            #Mute and an mute the audio
            pamixer -t
            if [ "$muteValue" == "false" ]
            then
                dunstify -h string:x-dunst-stack-tag:volume "$newValue % 🔇  " -a "Volume" -h int:value:"0" -t 2000
            else
                if (( "$newValue" > 65 ))
                then
                    dunstify -h string:x-dunst-stack-tag:volume "$newValue %  " -a "Volume" -h int:value:"$newValue" -t 2000 -u critical
                else
                    dunstify -h string:x-dunst-stack-tag:volume "$newValue %  " -a "Volume" -h int:value:"$newValue" -t 2000
                fi
            fi
            if [ -z "$playerctlStatus" ]
            then
               ~/.config/scripts/mediaNotification.sh -e
            else
                ~/.config/scripts/mediaNotification.sh -s
            fi
            ;;

        ?)
            exit 1
            ;; 
    esac
done
