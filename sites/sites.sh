#!/bin/bash

#load the option from the file sites.txt
locationScript="$HOME/.config/scripts"
allSites=( "F1TV"
"dysneyplus.com"
"netflix.com"
"ertflix"
"youtube.com"
"ytmusic"
"twitch.tv"
"odysee.com"
"f1"
"onedrive.com"
"bing maps"
"gdrive"
"gmaps"
"archlinux.org"
"protonMail"
"hotmail"
"thebikeguru"
"gitlab"
"github"
"word_online"
"emoticons"
"skroutz"
"bestprice"
"plaisio"
"msystems"
"exit_menu"
)

sites=$(printf "%s\n" "${allSites[@]}" | rofi -dmenu -theme /home/musicfan/.config/rofi/second_theme/dmenu_edited.rasi -p 'sites: ' -l 20)


#open f1 tv
if [ "$sites" == "F1TV" ]
then
	flatpak run org.chromium.Chromium --new-window https://f1tv.formula1.com/
fi

#open dysneyplus
if [ "$sites" == "dysneyplus.com" ]
then
	firefox -P Normal --new-window https://www.disneyplus.com/en-gb/home &
fi

#open youtube
if [ "$sites" == "youtube.com" ]
then
	firefox -P Normal --new-window youtube.com &
fi

#open twich
if [ "$sites" == "twitch.tv" ]
then
	firefox -P Normal --new-window twitch.tv &
fi

#open onedrive
if [ "$sites" == "onedrive.com" ]
then
	firefox -P Normal --new-window  onedrive.com &
fi

#open odysee
if [ "$sites" == "odysee.com" ]
then
	firefox -P Normal --new-window odysee.com &
fi

#open netflix
if [ "$sites" == "netflix.com" ]
then
	firefox -P Normal --new-window netflix.com &
fi

#open google drive
if [ "$sites" == "gdrive" ]
then
	firefox -P Normal --new-window  https://drive.google.com/drive/my-drive &
fi

#open archlinux page
if [ "$sites" == "archlinux.org" ]
then
	firefox -P Normal --new-window  archlinux.org &
fi

#open Microsoft Word (online version)
if [ "$sites" == "word_online" ] 
then 
    firefox -P Normal --new-window https://www.office.com/launch/word?ui=en-US&rs=US&auth=1 &
fi

#open music youtube
if [ "$sites" == "ytmusic" ]
then
	firefox -P  music --new-window &
fi

#open emoticons site
if [ "$sites" == "emoticons" ]
then
	firefox -P Normal https://emojidb.org/ &
fi

#open f1 Πρόγραμμα
if [ "$sites" == "f1" ]
then
	firefox -P Normal --new-window https://www.sofascore.com/el/motorsport/category/formula-1/40 &
fi

#open ertflix
if [ "$sites" == "ertflix" ]
then
	firefox -P Normal --new-window https://www.ertflix.gr/ &
fi

#open gitlab
if [ "$sites" == "gitlab" ]
then
	firefox -P Normal --new-window gitlab.com/musicfunfan &
fi

#open github
if [ "$sites" == "github" ]
then
	firefox -P Normal github.com &
fi

#open gmaps
if [ "$sites" == "gmaps"  ]
then
	firefox -P Normal --new-window https://www.google.com/maps/ &
fi

#open bing maps
if [ "$sites" == "bing maps" ]
then
	firefox -P Normal --new-window https://www.bing.com/maps &
fi

#open street maps
if [ "$sites" == "openstreetmap" ]
then
	firefox -P Normal --new-window https://www.openstreetmap.org/ &
fi

if [ "$sites" == "protonMail" ]
then
	firefox -P Mail --new-window https://account.proton.me/login &
fi


#Sart the hotmail in firefox
if [ "$sites" == "hotmail" ]
then
	firefox -P Mail --new-window https://outlook.live.com/owa/
fi


if [ "$sites" == "thebikeguru" ]
then
	firefox -P Normal --new-window https://thebikeguru.gr &
fi

#Open the skroutz site
if [ "$sites" == "skroutz" ]
then
	firefox -P Normal --new-window https://www.skroutz.gr/ &
fi

#Open the bestprice site_perl
if [ "$sites" == "bestprice" ]
then
	firefox -P Normal --new-window https://www.bestprice.gr/ &
fi

#Open plausio store site
if [ "$sites" == "plaisio" ]
then
	firefox -P Normal --new-window https://www.plaisio.gr/ &
fi

#Open msystems store site
if [ "$sites" == "msystems" ]
then
	firefox -P Normal --new-window https://www.msystems.gr/ &
fi




#check is a choice is made if not close the programm
if [ -z "$sites" ]
then
	notify-send "dmenu" "nothing is selected"
	exit 1
fi

#notification with what is selected
if [[ -n "$sites" ]]
then
	notify-send "dmenu" "this is $sites"
fi
        
	
#exit script 
if [ "$sites" == "exit_menu" ]
then
	exit 1
fi
