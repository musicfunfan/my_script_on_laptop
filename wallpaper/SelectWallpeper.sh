#!/bin/bash

#This is a script to change wallpapers.

#comments
#In order for this script to work you need to have the cron program like mine. You can find mine on my gitlab "gitlab.com/musicfunfan/"

#This is a method to select mode
selectMode ()

{
    echo "This is the selectMode"
    options=("stable" "changing" "exit")
    mode=$(printf "%s\n" "${options[@]}" | rofi -show -theme /home/musicfan/.config/rofi/second_theme/dmenu_edited.rasi -dmenu -i -l 3 -p "Selecr mode ")
    echo "This is the mode $mode"
}

#This is a method to find location of the files in mode chaging
changingDirectory ()

{
    options=("random" "in_order")
    directory=$(rofi -show file-browser-extended -theme "$HOME"/.config/rofi/decu_edited.rasi -q -file-browser-stdout -file-browser-only-dirs)
    modeChanging=$(printf "%s\n" "${options[@]}" | rofi -dmenu -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -i -l 2 -p "choose mode for changing" )
}

#This is a method to find the location of the files in mode srable
stableDirectory ()

{
    directory=$(rofi -show file-browser-extended -theme "$HOME"/.config/rofi/decu_edited.rasi -q -file-browser-stdout )
}

#This is to exit the script
exitScript ()

{
    exit 0
}

#This is a method to save the directory to text file for other scripts to acces
saveDirectory ()

{
    echo "$directory" > $HOME/.config/scripts/wallpeper
    if [ "$options" == "random" ]
    then
       touch "$HOME/.config/scripts/wallpaperMode"
       echo "random" > "$HOME/.config/scripts/wallpaperMode"
    fi

    if [ "$options" == "in_order" ]
    then
       touch "$HOME/.config/scripts/wallpaperMode"
       echo "in_order" > "$HOME/.config/scripts/wallpaperMode"
    fi
}

#This is a method to set the wallpaper
setWallpeperStable ()

{
    location=$(cat $HOME/.config/scripts/wallpeper)
    swaybg -m fit -i "$location" &
    echo "This is the location $location"
}

#This is a method to set the wallpaper
setWallpeperChanging ()
{
    location=$(cat $HOME/.config/scripts/wallpeper)
    if [ "$1" == "random" ]
    then
       wallpaper=$(ls "$location" |sort -R | awk 'FNR==1')
       swaybg -m fit -i "$directory"/"$wallpaper" &
    fi

    if [ "$1" == "in_order" ]
    then
       wallpaper=$(ls "$location" awk 'FNR==1')
       swaybg -m fit -i "$directory"/"$wallpaper" &
    fi
}

infinityLoop()
{
    echo "This is the infinityLoop is enabled"
    while :
    do
	mode=$(cat $HOME/.config/scripts/wallpaperMode)
	directory=$(cat $HOME/.config/scripts/wallpeper)
    location=$(cat $HOME/.config/scripts/wallpeper)
    if [ "$1" == "random" ]
    then
       killallSwaybg
       wallpaper=$(ls "$location" |sort -R | awk 'FNR==1')
       swaybg -m fit -i "$directory"/"$wallpaper" &
    fi

    if [ "$1" == "in_order" ]
    then
       killallSwaybg
       wallpaper=$(ls "$location" awk 'FNR==1')
       swaybg -m fit -i "$directory"/"$wallpaper" &
    fi
	sleep 1200
    done

}

#This is a method to kill all swaybg procces
killallSwaybg()
{
    killall swaybg
}


killallSwaybg
selectMode

if [ "$mode" == "changing" ]
then
    changingDirectory
    echo "this is the directory $directory"
    saveDirectory
    if [ "$modeChanging" == "random" ]
    then
       setWallpeperChanging "random"
       infinityLoop "random"
    else
        setWallpeperChanging "in_order"
        infinityLoop "in_order"
    fi
    exit 0
fi

if [ "$mode" == "stable"  ]
then
   stableDirectory
   echo "this is the directory $directory"
   saveDirectory
   setWallpeperStable
   exit 0
fi

if [ "$mode" == "exit" ]
then
   exit 0
fi

