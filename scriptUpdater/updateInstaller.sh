#!/usr/bin/bash

#This is a script to install and update my scriscript has been tested on LinuxMint(21) and archlinux Ubuntu 22.04.1  only. I working to test more oss.


#You need to have install the program lsb-release on your system before running this script

#Check the location of the script
echo "----------------------------------------------------------------------------------"
echo "//////////////////////////////////////////////////////////////////////////////////"
echo "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
echo "This script will not install Drivers for your hardware."
echo "The drivers that this script provide is what ever drivers you distro has installed"
echo "So please install your drivers for your hardware"
echo "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
echo "//////////////////////////////////////////////////////////////////////////////////"
echo "----------------------------------------------------------------------------------"
checkLocation ()
{
    locationOfScript=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
}

#Check the operetion system
checkOs ()
{
    osType=$(uname)
    if [ "$osType" == "Linux" ]
    then
        echo "combatible os detectet $osType"
        fullDistroId=$(lsb_release -i | grep "Distributor ID:" | awk {'print $3'})
        echo "This is the distro ID $fullDistroId"
    else
        echo "This os is not supported"
    fi
}


#This is a method to read all the dependenceis from the file
installAllDependenceis ()
{
    DependenceisArch=$(cat /home/$USER/gitrepo/my_scripts_ultimate/scriptUpdater/stuffToInstall.txt | awk '{print $1}')


    DependenceisMint=(
        kitty
        rofi-dev
        vim
        lxappearance
	    imagemagick
	    neofetch
	    libssl-dev
        pass
        autoconf
        gcc
        pkg-config
        libpam0g-dev
        libcairo2-dev
        libfontconfig1-dev
        libxcb-composite0-dev
        libev-dev
        libx11-xcb-dev
        libxcb-xkb-dev
        libxcb-xinerama0-dev
        libxcb-randr0-dev
        libxcb-image0-dev
        libxcb-util-dev
        libxcb-xrm-dev
        libxkbcommon-dev
        libxkbcommon-x11-dev
        libjpeg-dev
        pavucontrol
        libvterm-dev
        make
        terminator
        ranger
        barrier
        htop
        ssh
        lm-sensors
        qemu-kvm
        libvirt-daemon-system
        curl
        cmake
        libfreetype6-dev
	    libxcb-xfixes0-dev
	    python3
        rofi
        rsync
        git
        dmenu
        maim
        dunst
        playerctl
        radeontop
        feh
        mpv-mpris
        xdotool
        zenity
        jq
        rsync
        sway
        swaybg
        cargo
        feh
        firefox
        playerctl
        gnome-keyring
        dunst
        redshift
        radeontop
        polybar
        pipewire
        sddm
        blender
        obs-studio
        gnome-maps
        timeshift
        python3-pip
        emacs
        flatpak
)

DependenceisDebian=(
        timeshift
        wlsunset
        kitty
        rofi-dev
        vim
        lxappearance
	    imagemagick
	    neofetch
	    libssl-dev
        pass
        autoconf
        gcc
        pkg-config
        libpam0g-dev
        libcairo2-dev
        libfontconfig1-dev
        libxcb-composite0-dev
        libev-dev
        libx11-xcb-dev
        libxcb-xkb-dev
        libxcb-xinerama0-dev
        libxcb-randr0-dev
        libxcb-image0-dev
        libxcb-util-dev
        libxcb-xrm-dev
        libxkbcommon-dev
        libxkbcommon-x11-dev
        libjpeg-dev
        pavucontrol
        libvterm-dev
        make
        terminator
        ranger
        barrier
        htop
        ssh
        lm-sensors
        qemu-kvm
        libvirt-daemon-system
        curl
        cmake
        libfreetype6-dev
	    libxcb-xfixes0-dev
	    python3
        rofi
        rsync
        git
        dmenu
        maim
        dunst
        playerctl
        radeontop
        feh
        mpv
        xdotool
        zenity
        jq
        rsync
        sway
        swaybg
        cargo
        firefox-esr
        playerctl
        gnome-keyring
        dunst
        radeontop
        polybar
        pipewire
        sddm
        blender
        obs-studio
        gnome-maps
        python3-pip
        emacs
        flatpak
)

if [ "$fullDistroId" != "Arch" ] && [ "$fullDistroId" != "Ubuntu" ] && [ "$fullDistroId" != "Linuxmint" ] && [ "$fullDistroId" != "Debian" ]
    then
        echo "--------------------------------------------------------------------"
        echo "Your os $fullDistroId is not compatible with this script"
        echo "How ever you can try to use the configuration for some other os"
        echo "This is not qaranty that will work but you have nothing to loose."
        echo "If this is a test machine or a vm. any way... "
        echo "--------------------------------------------------------------------"
        echo
        echo "1) archlinux"
        echo "2) ubuntu"
        echo "3) linuxmint"
        echo "4) i do not like it please exit the scary script"

        read choice

    if [ "$choice" == "4" ]
    then
        exit 0
    fi
    if [ "$choice" == "1" ]
    then
        fullDistroId="Arch"
    elif [ "$choice" == "2" ]
    then
            fullDistroId="Ubuntu"
    elif [ "$fullDistroId" == "3" ]
    then
        fullDistroI="Linuxmint"
    fi
fi
echo "This is the fullDistroId now $fullDistroId"


    if [ "$fullDistroId" == "Debian" ]
    then
     updateDeb
     sudo apt install $(echo ${DependenceisDebian[@]})
     installDeb-get
     installDemufm
     installOpenRGB
     installElement
     installStarshipPromt
     installTopgrade
	 installDiscord
     installYt-dlp
     installDoomEmacs
     InstallBat
     installRofiFileBrowserExtended
     installLsd
     installCargoUpdate
     installFlatpak
     installEasyEffects
     installingQpwgraph

    elif [ "$fullDistroId" == "Arch" ]
    then
        echo "------------------"
        echo "Updating archlinux"
        echo "------------------"
        updateArch
        echo "----------------------------"
        echo "installing git,make,autoconf"
        echo "----------------------------"
        sudo pacman -S git make autoconf
        echo "----------------------"
        echo "Install YAY AUR HELPER"
        echo "----------------------"
        installYay
        echo "----------------------"
        echo "install stuff with yay"
        echo "----------------------"
        yay -S $DependenceisArch
        echo "-------------------------"
        echo "Activating networkManager"
        echo "-------------------------"
        ActivatingNetworkmanager
        echo "---------------"
        echo "Activating sddm"
        echo "---------------"
        ActivatingSddm

    elif [ "$fullDistroId" == "Linuxmint" ]
    then
         updateDeb
         sudo apt install $(echo ${DependenceisMint[@]})
         installDeb-get
         installDemufm
         installOpenRGB
         installElement
         installStarshipPromt
         installTopgrade
	     installDiscord
         installYt-dlp
         installDoomEmacs
         InstallBat
         installRofiFileBrowserExtended
         installLsd
         installCargoUpdate
         installFlatpak
         installEasyEffects
         installingQpwgraph
     elif [ "$fullDistroId" == "Ubuntu" ]
     then
         updateDeb
         sudo apt install $(echo ${DependenceisMint[@]})
         installDeb-get
         installDemufm
         installOpenRGB
         installElement
         installStarshipPromt
         installTopgrade
         installDiscord
         installYt-dlp
         installDoomEmacs
         installRofiFileBrowserExtended
         InstallBat
         installLsd
         installCargoUpdate
         installFlatpak
         installEasyEffects
         installingQpwgraph
    fi
}

#copy the files over in the "$HOME"/.config/scripts location
installScripts ()
{
	cd $locationOfScript; cd ..
	rsync -rvh ./*/* $HOME/.config/scripts/ --delete
}

#This is to install yay on archlinux
installYay ()
{
    mkdir "$HOME/gitrepo"
    cd "$HOME/gitrepo"
    pacman -S --needed git base-devel
    git clone https://aur.archlinux.org/yay.git
    cd yay
    makepkg -si
}

#Manual install dmenufm
installDemufm ()
{
	mkdir "$HOME/gitrepo"
	cd "$HOME/gitrepo"
    git clone https://github.com/huijunchen9260/dmenufm
    cd dmenufm
    sudo make install
}

#Installing my config files
installConfigs ()
{
    mkdir "$HOME/gitrepo"
    git clone https://gitlab.com/musicfunfan/main_configs_files $HOME/gitrepo/main_configs_files
    cp -r $HOME/gitrepo/main_configs_files/kitty $HOME/.config
    cp $HOME/gitrepo/main_configs_files/bash/.bashrc $HOME
    mkdir $HOME/.config/doom
    cp $HOME/gitrepo/main_configs_files/doom_emacs_configs/* $HOME/.config/doom
    cp -r $HOME/gitrepo/main_configs_files/dunst $HOME/.config
    cp -r $HOME/gitrepo/main_configs_files/sway $HOME/.config
    cp -r $HOME/gitrepo/main_configs_files/mpv $HOME/.config
    cp -r $HOME/gitrepo/main_configs_files/neofetch $HOME/.config
    cp -r $HOME/gitrepo/main_configs_files/waybar $HOME/.config
    cp -r $HOME/gitrepo/main_configs_files/ranger $HOME/.config
    cp -r $HOME/gitrepo/main_configs_files/rofi $HOME/.config
    cp -r $HOME/gitrepo/main_configs_files/topgrade $HOME/.config
    cp -r $HOME/gitrepo/main_configs_files/vim/.vimrc $HOME

    echo "---------------------------------------------------------"
    echo "Do you want to activate start up apps y/n ? DFAULT IS YES"
    echo "---------------------------------------------------------"
    read choice

    if [ "$choice" == "n" ]
    then
        echo "--------------------------------------"
        echo "Setting up the config for that setting"
        echo "--------------------------------------"
        openRgb="#exec openrgb --startminimized --profile profile_one_blue &"
        barrier="#exec barrier &"
        startUpApps="#exec --no-startup-id $HOME/.config/scripts/comms.sh"
        awk -v openRgb="$openRgb" 'NR==329 {print openRgb}'1 $HOME/.config/sway/config > $HOME/.config/sway/config.tmp
        awk -v barrier="$barrier" 'NR==340 {print barrier}'1 $HOME/.config/sway/config > $HOME/.config/sway/config.tmp
        awk -v startUpApps="$startUpApps" 'NR==343 {print startUpApps}'1 $HOME/.config/sway/config > $HOME/.config/sway/config.tmp
        mv $HOME/.config/sway/config.tmp $HOME/.config/sway/config
    else
        echo "--------------------------------"
        echo "default config will be installed"
        echo "--------------------------------"
    fi
}

#Installing the openrgb program
installOpenRGB ()
{
    deb-get install openrgb
}

#Installing element
installElement ()
{
    deb-get install element-desktop
}


#Installing starship promt
installStarshipPromt ()
{
	curl -sS https://starship.rs/install.sh | sh
    check=$(grep 'eval "\$(starship init bash)"' "$HOME/.bashrc")
    if [ "$check" ==  'eval "$(starship init bash)"' ]
    then
        echo "starship is already in the Bash PATH"
    else
        echo "Adding starship to the PATH"
        echo 'eval "$(starship init bash)"' >> $HOME/.bashrc
    fi
}

#installing topgrade
installTopgrade ()
{
    cargo install topgrade
}

#installling wallpapers
installWallpepers()
{
    mkdir $HOME/gitrepo
    mkdir $HOME/icons
    echo "This is wallpapers stuff"
    git clone https://gitlab.com/dwt1/wallpapers $HOME/gitrepo/wallpapers
    rm $HOME/gitrepo/wallpapers/README.md
    sudo rm -r $HOME/gitrepo/wallpapers/.git
    cp -r $HOME/gitrepo/wallpapers $HOME/icons
}

#installing Discord
installDiscord ()
{
    deb-get install discord
}

#Adding cargo bins in the path
addCargoBins ()
{	
	whatCheck=(PATH=$HOME/.cargo/bin:\$PATH)
	check=$(grep $whatCheck "$HOME/.bashrc")
    if [ -z "$check" ]
    then
	 echo "Adding cargo to the PATH"
        echo "PATH=$HOME/.cargo/bin:\$PATH" >> $HOME/.bashrc
else 
	echo "cargo bin is allready in the PATH"
    fi
}

#Installing myDmenuConfig
installingMyDmenu ()
{
    mkdir $HOME/gitrepo
    cd $HOME/gitrepo
    git clone https://gitlab.com/musicfunfan/my_dmenu
    cd my_dmenu
    sudo make install
}

#Installing deb-get
installDeb-get ()
{
    echo "This is installing dep-get"
    curl -sL https://raw.githubusercontent.com/wimpysworld/deb-get/main/deb-get | sudo -E bash -s install deb-get
}

#updating the main system
updateDeb ()
{
    sudo apt update
    sudo apt upgrade
}

#updating the main system arch
updateArch ()
{
    echo "------------------"
    echo "updating archlinux"
    echo "------------------"
    sudo pacman -Suy
}

#installing yt-dlp
installYt-dlp ()
{
    pip install yt-dlp
}

#installing doom emacs
installDoomEmacs ()
{
    git clone --depth 1 --single-branch https://github.com/doomemacs/doomemacs ~/.config/emacs
    ~/.config/emacs/bin/doom install
}

#installing rofi-file-browser-extended
installRofiFileBrowserExtended ()
{
	echo "This is the rofi-file-browser-exrended"
    git clone https://github.com/marvinkreis/rofi-file-browser-extended $HOME/gitrepo/rofi-file-browser-extended
    cd $HOME/gitrepo/rofi-file-browser-extended
    cmake .
    make
    sudo make install
}

#installing my crontab
installCrontab ()
{
    crontab $HOME/gitrepo/my_config_files_for_linux/cron/my_crontab.txt
}

#installing virtual machine software
InstallVm ()
{
    sudo adduser $USER libvirt
}

#Installing bat command
InstallBat ()
{
    deb-get install bat
}

#installing lsd
installLsd ()
{
    deb-get install lsd
}

#read sesnors to get cpu temp
cpuTempRead ()
{
    echo "Choose a sesnore to display the temp of the cpu"
    sensors
    read choice
    echo "Select the column you want to out put on the bar"
    read colum
    echo "This is the choice of the cpu sensors $choice"
    configPart="exec = sensors |grep $choice | awk '{print \$$colum}'"

    awk -v configPart="$configPart" 'NR==194{print configPart}'1 $HOME/.config/polybar/config.ini > $HOME/.config/polybar/config.ini.tmp
    mv $HOME/.config/polybar/config.ini.tmp $HOME/.config/polybar/config.ini
}

#Adding location for wlsunset
LocationAdd ()
{
    echo "--------------------------------------------------"
    echo
    echo "Do you want wlsunset to be enable by default ? y/n"
    echo
    echo "--------------------------------------------------"
    read choice
    if [ "$choice" == "y" ]
    then
        echo "Give me the latitube"
        read latitube
        echo "Gine me the Longittube"
        read longitube
        awk -v location="$location" 'NR==321{print "exec wlsunset -l" latitube " -L " longitube }'1 $HOME/.config/sway/config > $HOME/.config/sway/config.tmp
	mv $HOME/.config/sway/config.tmp $HOME/.config/sway/config
    else
        echo "Ok wlsunset will not be activated. If you want you can acrivated manualy"
    fi

}

#Installin cargo-update
installCargoUpdate ()
{
	echo "-----------------------"
	echo "Installing cargo-update"
	echo "-----------------------"
    cargo install cargo-update
}

#install easy effects
installEasyEffects ()
{
    flatpak install easyeffects
}

#install flatpak
installFlatpak ()
{
    sudo apt install flatpak
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
}

#install qpwgraph
installingQpwgraph ()
{
    flatpak install qpwgraph
}

#Ask and take a snaphot usint timeshift before run the main script
snapshot ()
{
    echo "-----------------------------------------------------------------------------------------------------------"
    echo "Do you wan to take a snapshot ? Before running this big script from a guy on gitlab.com ;) y/n Default is n"
    echo "-----------------------------------------------------------------------------------------------------------"
    read choice
    if [ "$choice" == "y" ]
    then

         isTimeshiftHere=$(whereis timeshift | grep bin)

         if [ -e "$isTimeshiftHere" ]
          then
              echo "-----------------"
              echo "/////////////////"
              echo "Timeshift is here"
              echo "/////////////////"
              echo "-----------------"
              echo
              echo "-------------------"
              echo "Taking snapshot...."
              echo "-------------------"

              sudo timeshift --create

         else
             echo "---------------------"
             echo "Timeshift is not here"
             echo "---------------------"
             echo
             echo "Installing timeshift"
             echo "--------------------"

             echo
         fi


        if [ "$fullDistroId" == "Linuxmint" ] || [ "$fullDistroId" == "Ubuntu" ]
        then
            sudo apt install timeshift
            echo "----------------------------"
            echo "Timeshift has been installed"
            echo "----------------------------"
            echo
            echo "------------------------------------"
            echo "Running timeshift to create snapshot"
            echo "------------------------------------"
            sudo timeshift --create

        elif [ "$fullDistroId" == "Arch" ]
        then
            echo "----------------------------------------------------------------------------"
            echo "You are using archlinux timeshift is not in the reqular arch linux repo"
            echo "Please install it via AUR then run the script if you want to take a snapshot"
            echo "----------------------------------------------------------------------------"
        fi
    fi
}

#installing skypeforlinux
InstallSkypeForLinux ()
{
	deb-get install skypeforlinux
}

#activating sddm
ActivatingSddm ()
{
    echo "---------------"
    echo "activating sddm"
    echo "---------------"
    sudo systemctl enable sddm
}

#Activating network manager
ActivatingNetworkmanager ()
{
    echo "-------------------------"
    echo "activating NetworkManager"
    echo "-------------------------"
    sudo systemctl enable NetworkManager
}

echo "--------------------------------------"
echo "Cheking the location of the script"
echo "--------------------------------------"
checkLocation
echo "--------------------------------------"
echo "Cheking the os"
echo "--------------------------------------"
checkOs
echo "--------------------------------------"
echo "Taking snapshot"
echo "--------------------------------------"
snapshot
installAllDependenceis
echo "--------------------------------------"
echo "Install configs"
echo "--------------------------------------"
installConfigs
echo "--------------------------------------"
echo "Install scripts"
echo "--------------------------------------"
installScripts
echo "--------------------------------------"
echo "Install wallpapers" 
echo "--------------------------------------"
installWallpepers
echo "--------------------------------------"
echo "Install crontab" 
echo "--------------------------------------"
installCrontab
echo "--------------------------------------"
echo "Add cargo bins on the PATH" 
echo "--------------------------------------"
addCargoBins
echo "--------------------------------------"
echo "Set the cpu temp reading" 
echo "--------------------------------------"
cpuTempRead
echo "--------------------------------------"
echo "Set the redshift location" 
echo "--------------------------------------"
LocationAdd
echo "--------------------------------------"
echo "Set the default terminal emulator"
echo "--------------------------------------"
