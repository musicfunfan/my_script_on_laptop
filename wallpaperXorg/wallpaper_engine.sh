#!/bin/bash

#this a script to change the wallpaper randomly 


#change to a random wallpaper 

location=$(cat "$HOME/.config/scripts/wallpeper")
mode=$(cat "$HOME/.config/scripts/wallpaperMode")
echo "$mode"
echo "$location" 
if [ "$mode" == "in_order" ]
then
	feh --bg-center "$location"/*
fi

if [ "$mode" == "random" ]
then
	feh --bg-center --randomize "$location"/*
fi
