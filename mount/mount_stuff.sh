#!/bin/bash

#This is a script that mount hard drives on the system.

#This is a function to get all the connected drives

getAllDevice ()
{
    echo "This is the method to get all the disk connected on the computer"
    echo "Starting"
    connectedDevices=$(lsblk -o name,label,fstype,size,mountpoints,model)
    options=("$connectedDevices")
    num=$(echo "$connectedDevices" | wc -l)
    numOptions=$(("$num + 1"))
    choice=$(printf "%s\n" "${options[@]}" | dmenu -i -l "$numOptions" -p "choose a device to mount :")
    echo "This is the choice $choice"
    echo "done"
}

#This is the method to get the partision to mount
getThePartision ()
{
    echo "This is the method to get the partision to mound"
    echo "Starting"
    partisionToMount=$(echo "$choice" | awk '{print substr($1,3); }')
    echo "This is the partisionToMount $partisionToMount"
    echo "done"
}

#This is a method to find the mount points
getTheMountPoint ()
{
    echo "This is the mehtod to get the mountpoints"
    echo "Starting"
    mountPoint=$(find /media/$USER -maxdepth 1)
    num=$(echo "$mountPoint" | wc -l)
    echo "this is the mount points $mountPoint"
    chooseMountPoint=$(echo "$mountPoint" |dmenu -i -l "$num" -p "choose mount point :")
    echo "This is the choose mount point $chooseMountPoint"
    echo "done"
}

#This is the a method to mount the partision with the mount point
mountThePartision ()
{
    echo "This is the method to mount the partision"
    echo "start"
    pkexec mount /dev/"$partisionToMount" "$chooseMountPoint" 
    echo "done"
}

#This is a method to exit the scripr
exitScript ()
{
    exit 1
}

#delete mount points
deleteMountPoints ()
{

    echo "This is in development"
    echo "This is the $chooseMountPoint"
    directoryCheck=$(ls -A "$chooseMountPoint")
    echo "This is the directoryCheck $directoryCheck"
    
    if [ "$directoryCheck" == ""  ]
    then
        notify-send "mount script" "The mount point will be deleted"
        pkexec rm -r "$chooseMountPoint"
        
    else 
        notify-send "mount script" "The mount point can not be deleted please umount it !!"
    fi
}

#create mount points
createMountPoints ()
{
    echo "this is in development"
    newMountPoint=$(rofi -dmenu -i -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -p "Type the name of the mount point")
    userName=$(whoami)
    pkexec mkdir /media/"$userName"/"$newMountPoint"
}



#This is the a method to umount the partision with the mount point
umountThePartision ()
{
    echo "This is the method to mount the partision"
    echo "start"
    pkexec umount "$chooseMountPoint" 
    echo "done"
}

#Calling the methods above

options=("mount a device" "umount device" "create new mount point" "delete mount point" "exit_menu" )

operation=$(printf "%s\n" "${options[@]}" | rofi -dmenu -l 5 -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -p "Choose an operation")
if [ "$operation" == "mount a device" ]
then
    getAllDevice
    getThePartision
    getTheMountPoint
    mountThePartision   
    exitScript
elif [ "$operation" == "create new mount point" ]
then
    createMountPoints
    exitScript
elif [ "$operation" == "delete mount point" ]
then 
    getTheMountPoint
    deleteMountPoints
    exitScript
elif [ "$operation" == "exit_menu" ]
then
exitScript
elif [ "$operation" == "umount device" ]
then
    getTheMountPoint
    umountThePartision
    exitScript
fi
exitScript
