#!/bin/bash


#Countdown for the screenshot
timeToDelay=$(echo "Type the value in second" | dmenu -i -l 20 -p 'How much time to you want: ')
#Open a dmenu to ask the user if he want to save the screenshot
scriptLocation="$HOME/.config/scripts"
choiceSave=$(less $scriptLocation/choiceSave.txt |dmenu -i -l 20 -p 'Where to save ?: ')


# If the user does not want to save the screenshot do this:
if [ "$choiceSave" == "clip" ]
then
    
    #Open a dmenu to ask the user what type of screenshot they want 
    choice=$(less $scriptLocation/screenshot.txt |dmenu -i -l 20 -p 'Type: ' )
    
    #If the type is only the focus window do this:
    if [ "$choice" == "target" ]
    then
        sleep $timeToDelay
        grim -g "$(swaymsg -t get_tree | jq -r '.. | select(.focused?) | .rect | "\(.x),\(.y) \(.width)x\(.height)"')" - | wl-copy
    fi

    #If the type is the full screen do this:
    if [ "$choice" == "all" ]
    then
        sleep $timeToDelay
        grim - | wl-copy
    fi
    
    #If the type is selected area do this:
    if [ "$choice" == "more" ]
    then
        sleep $timeToDelay
        grim -g "$(slurp)" - | wl-copy
    fi

#If the user want to save the screenshot in the home folder the to do this:
elif [ "$choiceSave" == "home" ] 
then    
    echo "This is the save in home"
    #Open a dmenu to ask the user what type of screenshot they want 
    choice=$(less $scriptLocation/screenshot.txt |dmenu -i -l 20 -p 'Type: ')
    
    #If the type is only the focus window do this:
    if [ "$choice" == "target" ]
    then
        sleep $timeToDelay
        grim -g "$(swaymsg -t get_tree | jq -r '.. | select(.focused?) | .rect | "\(.x),\(.y) \(.width)x\(.height)"')" /home/$USER/screenshot.png
    fi 
    
    #If the type is the full screen do this:
    if [ "$choice" == "all" ]
    then
        sleep $timeToDelay
        grim /home/$USER/screenshot.png
    fi
    
    #If the type is selected area do this:
    if [ "$choice" == "more" ]
    then
        sleep $timeToDelay
        grim -g "$(slurp)" /home/$USER/screenshot.png
    fi

#If the user want to save in a custom location so this
elif [ "$choiceSave" == "custom local" ]
then
    
    #Open a dmenu to ask the user what type of screenshot they want 
    choice=$(less $scriptLocation/screenshot.txt | dmenu -i -l 20 -p 'Type: ')
    
    #Open file picker to ask the user where to save the screenshot
    choiceLocation=$(zenity --file-selection --directory)
    
    #Ask the user the name of the file
    choiceName=$(less $scriptLocation/cheater.txt | dmenu -i -l 20 -p 'give a name for the file: ')
    
    #If the user does not want to use a custom name do:
    if [ "$choiceName" == "use the default name" ]
    then
        sleep $timeToDelay
        choiceName="screenshot"
    fi
    
    #If the type is only the focus window do this:
    if [ "$choice" == "target" ] && [ "$choiceName" != "exit_menu" ]
    then
        sleep $timeToDelay
        grim -g "$(swaymsg -t get_tree | jq -r '.. | select(.focused?) | .rect | "\(.x),\(.y) \(.width)x\(.height)"')" $choiceLocation/$choiceName
    fi
    
    #If the type is the full screen do this:
    if [ "$choice" == "all" ] && [ "$choiceName" != "exit_menu" ]
    then
        sleep $timeToDelay
        grim $choiceLocation/$choiceName
    fi
    
    #If the type is selected area do this:
    if [ "$choice" == "more" ] && [ "$choiceName" != "exit_menu" ]
    then
        sleep $timeToDelay
        grim -g "$(slurp)" $choiceLocation/$choiceName
    fi

# No optios has selected do this:
else 
    dunstify -a "screenshotScript" "Screenshot script" "script close"
    exit 1
fi

#exit menu
if [ "$choice" == "exit_menu" ] || [ "$choiceSave" == "exit_menu" ] || [ "$choiceName" == "exit_menu" ]
then
    exit 1
fi

dunstify -a "screenshotScript" "Screenshot script" "screenshot is ready"
exit 1
